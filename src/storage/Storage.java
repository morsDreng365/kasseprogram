package storage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import model.Produkt;
import model.ProduktGruppe;
import model.ProduktMenu;
import model.Vare;

public class Storage {

	private static Storage instance = null;

	private ArrayList<Produkt> produkter = new ArrayList<Produkt>();
	private ArrayList<ProduktGruppe> produktGrupper = new ArrayList<ProduktGruppe>();
	private ArrayList<Vare> varer = new ArrayList<Vare>();
	private ArrayList<ProduktMenu> produktMenuer = new ArrayList<ProduktMenu>();

	public Storage() {
	}

	private static boolean saving = true;

	public static void setSaving(boolean saving) {
		Storage.saving = saving;
	}

	public static Storage getInstance() {
		if (saving && instance == null) {
			instance = loadStorage();
		}
		if (instance == null) {
			instance = new Storage();
			System.out.println("Empty storage created");
		}
		return instance;
	}

	public ArrayList<Produkt> getAllProdukter() {
		return new ArrayList<>(produkter);
	}

	public ArrayList<ProduktGruppe> getAllProdukteGrupper() {
		return new ArrayList<>(produktGrupper);
	}

	public ArrayList<Vare> getAllVarer() {
		return new ArrayList<>(varer);
	}

	public ArrayList<ProduktMenu> getAllProduktMenuer() {
		return new ArrayList<>(produktMenuer);
	}

	public void addProdukter(Produkt produkt) {
		if (!produkter.contains(produkt)) {
			produkter.add(produkt);
		}
	}

	public void removeProdukter(Produkt produkt) {
		if (produkter.contains(produkt)) {
			produkter.remove(produkt);
		}
	}

	public void addProduktGrupper(ProduktGruppe grupper) {
		if (!produktGrupper.contains(grupper)) {
			produktGrupper.add(grupper);
		}
	}

	public void removeProduktGrupper(ProduktGruppe grupper) {
		if (produktGrupper.contains(grupper)) {
			produktGrupper.remove(grupper);
		}
	}

	public void addVare(Vare vare) {
		if (!varer.contains(vare)) {
			varer.add(vare);
		}
	}

	public void removeaVare(Vare vare) {
		if (varer.contains(vare)) {
			varer.remove(vare);
		}
	}

	public void addProduktMenu(ProduktMenu menuer) {
		if (!produktMenuer.contains(menuer)) {
			produktMenuer.add(menuer);
		}
	}

	public void removeProduktMenu(ProduktMenu menuer) {
		if (produktMenuer.contains(menuer)) {
			produktMenuer.remove(menuer);
		}
	}

	public static Storage loadStorage() {
		String fileName = "storage.ser";
		try (FileInputStream fileIn = new FileInputStream(fileName);
				ObjectInputStream in = new ObjectInputStream(fileIn)) {
			Storage instance = (Storage) in.readObject();
			System.out.println("Storage loaded from file " + fileName);
			return instance;
		} catch (ClassNotFoundException ex) {
			System.out.println("Storage object NOT loaded from file " + fileName);
			System.out.println("Exception: " + ex);
		} catch (IOException ex) {
			System.out.println("Storage object NOT loaded from file " + fileName);
			System.out.println("Exception: " + ex);
		}
		return null;
	}

	public static void saveStorage() {
		String fileName = "storage.ser";
		try (FileOutputStream fileOut = new FileOutputStream(fileName);
				ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
			out.writeObject(instance);
			System.out.println("Storage saved in file " + fileName);
		} catch (IOException ex) {
			System.out.println("ERROR saving storage object in file " + fileName);
			System.out.println("Exception: " + ex);
			throw new RuntimeException(ex);
		}
	}

}
