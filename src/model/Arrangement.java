package model;

import java.time.LocalDate;

public class Arrangement {

	private Prisliste prisListe;
	private LocalDate startDato;
	private LocalDate slutDato;

	public Arrangement(Prisliste prisListe, LocalDate startDato, LocalDate slutDato) {
		this.prisListe = prisListe;
		this.startDato = startDato;
		this.slutDato = slutDato;
	}

	public Prisliste getPrisListe() {
		return prisListe;
	}

	public LocalDate getStartDato() {
		return startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}
}
