package model;

public enum ProduktGruppe {
	Flaskeøl, Fadøl, Spiritus, Fustage, Kulsyre, Malt, Beklædning, Anlæg, Glas, Sampakning, Rundvisning
}
