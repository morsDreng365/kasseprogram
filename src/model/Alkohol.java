package model;

public class Alkohol extends Produkt {

	// ændret fra int til double
	private double alkholProcent;
	private int cl;
	private int klip;

	public Alkohol(String navn, String beskrivelse, double pris, int antalPaLager, double alkoholProcent, int cl,
			int klip) {
		super(navn, beskrivelse, pris, antalPaLager);
		this.alkholProcent = alkoholProcent;
		this.cl = cl;
		this.klip = klip;
	}

	public double getAlkholProcent() {
		return alkholProcent;
	}

	public int getCl() {
		return cl;
	}

	public int getKlip() {
		return klip;
	}

}
