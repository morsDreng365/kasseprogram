package model;

import java.time.*;

public class Rundvisning {
	LocalDate dato;
	LocalTime tid;

	public Rundvisning(LocalDate dato, LocalTime tid) {
		this.dato = dato;
		this.tid = tid;
	}

	public LocalDate getDato() {
		return dato;
	}

	public void setDato(LocalDate dato) {
		this.dato = dato;
	}

	public LocalTime getTid() {
		return tid;
	}

	public void setTid(LocalTime tid) {
		this.tid = tid;
	}

}
