package model;

public class AlkoholMedPant extends Alkohol {
	double pant;

	public AlkoholMedPant(String navn, String beskrivelse, double pris, double alkoholProcent, int cl, double pant) {
		super(navn, beskrivelse, pris, alkoholProcent, cl);
		this.pant = pant;

	}

	public double getPant() {
		return pant;
	}

	public void setPant(double pant) {
		this.pant = pant;
	}

}
