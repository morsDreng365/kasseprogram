package model;

public enum Betalingsform {
	KREDITKORT, MOBILEPAY, KONTANT, KLIPPEKORT
}
