package model;

public abstract class ProduktMenu {

	// Repetivit at have navn her når produkt og produktgruppe allerede har et navn?
	public abstract String getNavn();

	public abstract void print();

	public abstract void addPrisliste(ProduktMenu p);
}
