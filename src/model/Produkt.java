package model;

public class Produkt extends ProduktMenu {

	private String navn;
	private String beskrivelse;
	private double pris;

	public Produkt(String navn, String beskrivelse, double pris) {
		this.navn = navn;
		this.beskrivelse = beskrivelse;
		this.pris = pris;
	}

	public String getNavn() {
		return navn;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public double getPris() {
		return pris;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addPrisliste(ProduktMenu p) {
		// TODO Auto-generated method stub

	}
}
